CREATE TABLE `cliente`(
    `id` bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `uuid` varchar(36) NOT NULL,
    `nome` VARCHAR(150) NOT NULL,
    `cpf` VARCHAR(11) NOT NULL,
    `data_nascimento` DATE NOT NULL
)ENGINE=InnoDB;

