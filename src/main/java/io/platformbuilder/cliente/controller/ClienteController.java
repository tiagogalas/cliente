package io.platformbuilder.cliente.controller;

import io.platformbuilder.cliente.modelo.Cliente;
import io.platformbuilder.cliente.modelo.PropAtualizacao;
import io.platformbuilder.cliente.service.ClienteService;
import io.platformbuilder.cliente.utils.filter.ClienteFilter;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiago
 */
@RestController
@RequestMapping("/clientes")
public class ClienteController {
    
    @Autowired
    private ClienteService service;
    
    @GetMapping
    public Page<Cliente> todosClientes(Pageable page,
            @RequestParam(required = false) String nome, @RequestParam(required = false) String cpf){
        ClienteFilter filter = new ClienteFilter(nome, cpf);
        return service.buscarClientesPorFiltro(page, filter);
    }
    
    @PostMapping
    public ResponseEntity<Cliente> gravarNovoCliente(@Valid @RequestBody Cliente cliente){
        cliente = this.service.gravarCliente(cliente);
        return ResponseEntity.status(HttpStatus.CREATED).body(cliente);
    }
    
    @PutMapping("/{uuid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizarCliente(@RequestBody Cliente cliente, @PathVariable String uuid){
        this.service.atualizarCliente(cliente, uuid);
    }
    
    @DeleteMapping("/{uuid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removerCliente(@PathVariable String uuid){
        this.service.removerCliente(uuid);
    }
    
    @PatchMapping("/{uuid}/{prop}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizaParcial(@PathVariable String uuid, @PathVariable PropAtualizacao prop,
            @RequestBody Cliente cliente){
        this.service.atualizarParcial(uuid,prop,cliente);
    }
    
    
    
}
