package io.platformbuilder.cliente.utils.filter;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author tiago
 */
@Data
@AllArgsConstructor
public class ClienteFilter {
    private String nome;
    private String cpf;
    
}
