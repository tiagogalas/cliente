package io.platformbuilder.cliente.repository;

import io.platformbuilder.cliente.modelo.Cliente;
import io.platformbuilder.cliente.repository.query.ClienteRepositoryQuery;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tiago
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>, ClienteRepositoryQuery{
    
    public Optional<Cliente> findByUuid(String uuid);
    public Optional<Cliente> findByCpf(String cpf);
    
}
