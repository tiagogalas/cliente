package io.platformbuilder.cliente.repository.query;

import io.platformbuilder.cliente.modelo.Cliente;
import io.platformbuilder.cliente.utils.filter.ClienteFilter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author tiago
 */
public class ClienteRepositoryImpl implements ClienteRepositoryQuery {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Page<Cliente> buscarClientesPorFiltro(Pageable pageable, ClienteFilter filter) {
        CriteriaBuilder builder = this.manager.getCriteriaBuilder();
        CriteriaQuery<Cliente> criteria = builder.createQuery(Cliente.class);
        Root<Cliente> root = criteria.from(Cliente.class);

        Predicate[] predicates = criarPredicates(filter, builder, root);
        criteria.where(predicates);

        TypedQuery<Cliente> query = manager.createQuery(criteria);
        addPaginacao(query, pageable);
        return new PageImpl<>(query.getResultList(), pageable, total(filter));
    }

    private Predicate[] criarPredicates(ClienteFilter filter, CriteriaBuilder builder, Root<Cliente> root) {
        List<Predicate> predicates = new ArrayList<>();

        if (filter.getNome() != null) {
            predicates.add(builder.like(builder.lower(root.get("nome")),
                    "%" + filter.getNome().toLowerCase() + "%"));
        }
        if(filter.getCpf() != null){
            predicates.add(builder.equal(builder.lower(root.get("cpf")),
                    filter.getCpf()));
        }
        return predicates.toArray(new Predicate[predicates.size()]);

    }

    private void addPaginacao(TypedQuery<Cliente> query, Pageable pageable) {
        int paginaAtual = pageable.getPageNumber();
        int totalRegistrosPorPagina = pageable.getPageSize();
        int primeiroRegistroPagina = paginaAtual * totalRegistrosPorPagina;

        query.setFirstResult(primeiroRegistroPagina);
        query.setMaxResults(totalRegistrosPorPagina);

    }

    private Long total(ClienteFilter filter) {
        CriteriaBuilder builder = this.manager.getCriteriaBuilder();
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        Root<Cliente> root = criteria.from(Cliente.class);

        Predicate[] predicates = criarPredicates(filter, builder, root);

        criteria.where(predicates);
        criteria.select(builder.count(root));
        return manager.createQuery(criteria).getSingleResult();
    }

}
