package io.platformbuilder.cliente.repository.query;

import io.platformbuilder.cliente.modelo.Cliente;
import io.platformbuilder.cliente.utils.filter.ClienteFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author tiago
 */
public interface ClienteRepositoryQuery {
    
    public Page<Cliente> buscarClientesPorFiltro(Pageable pageable, ClienteFilter filter);
}
