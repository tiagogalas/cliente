package io.platformbuilder.cliente.service.impl;

import io.platformbuilder.cliente.modelo.Cliente;
import io.platformbuilder.cliente.modelo.PropAtualizacao;
import io.platformbuilder.cliente.repository.ClienteRepository;
import io.platformbuilder.cliente.service.ClienteService;
import io.platformbuilder.cliente.service.exception.ClienteNaoEncontradoException;
import io.platformbuilder.cliente.utils.ValidacaoClienteUtils;
import io.platformbuilder.cliente.utils.filter.ClienteFilter;
import java.util.UUID;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author tiago
 */
@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private ValidacaoClienteUtils validacao;

    @Override
    public Page<Cliente> buscarClientesPorFiltro(Pageable pageable, ClienteFilter filter) {
        return this.repository.buscarClientesPorFiltro(pageable, filter);
    }

    @Override
    public Cliente gravarCliente(Cliente cliente) {
        cliente.setUuid(UUID.randomUUID().toString());
        validacao.validarCliente(cliente);
        if (!this.repository.findByCpf(cliente.getCpf()).isPresent()) {
            return this.repository.save(cliente);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "O cliente com cpf" + cliente.getCpf() + "já está cadastrado!");
        }
    }

    @Override
    public Cliente buscarClientePorUuid(String uuid) {
        return buscarCliente(uuid);       
    }

    @Override
    public Cliente atualizarCliente(Cliente cliente, String uuid) {
        Cliente clienteTemp = buscarCliente(uuid);
        BeanUtils.copyProperties(cliente, clienteTemp, "id", "uuid");
        validacao.validarCliente(clienteTemp);
        return this.repository.save(clienteTemp);
    }

    @Override
    public boolean removerCliente(String uuid) {
        Cliente cliente = buscarCliente(uuid);
        this.repository.delete(cliente);
        return true;
    }

    @Override
    public void atualizarParcial(String uuid, PropAtualizacao prop, Cliente cliente) {
        Cliente clienteTemp = buscarCliente(uuid);
        switch(prop){
            case NOME:
                clienteTemp.setNome(cliente.getNome());
                break;
            case CPF:
                clienteTemp.setCpf(cliente.getCpf());
                break;
            case DATA_NASCIMENTO:
                clienteTemp.setDataNascimento(cliente.getDataNascimento());
                break;
            default:
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Selecione uma propriedade válida!");                    
        }
        validacao.validarCliente(clienteTemp);
        this.repository.save(clienteTemp);
    }

    private Cliente buscarCliente(String uuid) {
        return this.repository.findByUuid(uuid)
                .orElseThrow(ClienteNaoEncontradoException::new);
    }
}
