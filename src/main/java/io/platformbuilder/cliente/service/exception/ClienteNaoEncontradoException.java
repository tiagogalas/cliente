package io.platformbuilder.cliente.service.exception;

/**
 *
 * @author tiago
 */
public class ClienteNaoEncontradoException extends RuntimeException{

    public ClienteNaoEncontradoException(String string) {
        super("Cliente não encontrado: " + string);
    }

    public ClienteNaoEncontradoException() {
        super("Cliente não encontrado");
    }
    
    
}
