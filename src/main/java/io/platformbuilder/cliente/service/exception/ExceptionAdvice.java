package io.platformbuilder.cliente.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author tiago
 */
@ControllerAdvice
public class ExceptionAdvice {
    
    @ResponseBody
    @ExceptionHandler(ClienteNaoEncontradoException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String clienteNaoEncontradoHandler(ClienteNaoEncontradoException ex){
        return ex.getMessage();
    }
    
    @ResponseBody
    @ExceptionHandler(ClienteJaCadastradoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String ClienteJaCadastradoException(ClienteJaCadastradoException ex){
        return ex.getMessage();
    }
}
