package io.platformbuilder.cliente.service.exception;

/**
 *
 * @author tiago
 */
public class ClienteJaCadastradoException extends RuntimeException{

    public ClienteJaCadastradoException(String msg) {
        super(msg);
    }
    
}
