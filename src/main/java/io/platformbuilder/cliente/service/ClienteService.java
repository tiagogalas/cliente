package io.platformbuilder.cliente.service;

import io.platformbuilder.cliente.modelo.Cliente;
import io.platformbuilder.cliente.modelo.PropAtualizacao;
import io.platformbuilder.cliente.utils.filter.ClienteFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author tiago
 */
public interface ClienteService {
    
    public Cliente gravarCliente(Cliente cliente);
    public Cliente atualizarCliente(Cliente cliente, String id);
    public boolean removerCliente(String uuid);
    public Page<Cliente> buscarClientesPorFiltro(Pageable pageable, ClienteFilter filter);
    public Cliente buscarClientePorUuid(String uuid);
    public void atualizarParcial(String uuid, PropAtualizacao prop, Cliente cliente);
    
}
