package io.platformbuilder.cliente.modelo;

/**
 *
 * @author tiago
 */
public enum PropAtualizacao {
    NOME,
    CPF,
    DATA_NASCIMENTO
}
