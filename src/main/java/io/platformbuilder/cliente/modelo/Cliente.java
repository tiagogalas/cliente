package io.platformbuilder.cliente.modelo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.platformbuilder.cliente.utils.DataUtils;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author tiago
 */
@Entity
@Table(name = "cliente")
@Data
public class Cliente {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "uuid", length = 36, nullable = false)
    private String uuid;
    
    @Column(name = "nome", length = 150, nullable = false)
    private String nome;
    
    @Column(name = "cpf", length = 11, nullable = false)
    private String cpf;
    
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    @Column(name = "data_nascimento", nullable = false)
    private Date dataNascimento;
    
    @Transient
    private int idade;
    
    @PostLoad
    public void calcularIdade(){
        this.idade = DataUtils.calcularIdade(dataNascimento);
    }
    
}
