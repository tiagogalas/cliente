package io.platformbuilder.cliente.utils;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author tiago
 */
public class DataUtils {
    public static int calcularIdade(Date nascimento){
        if(nascimento != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(nascimento);
            return Period.between(LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH)), 
                    LocalDate.now()).getYears();
        }
        return 0;
    }
}
