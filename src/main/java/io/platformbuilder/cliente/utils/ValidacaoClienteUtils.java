package io.platformbuilder.cliente.utils;

import io.platformbuilder.cliente.modelo.Cliente;
import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author tiago
 */
@Service
public class ValidacaoClienteUtils {

    public void validarCliente(Cliente cliente) {
        if(cliente.getNome() == null
                || cliente.getNome().isEmpty()
                || cliente.getNome().length() > 150){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, 
                    "O nome do cliente não pode ser vazio ou conter mais de 150 caracteres!");
        }
        if(cliente.getCpf()== null || cliente.getCpf().length() != 11){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, 
                    "CPF Inválido!");
        }
        
        if(cliente.getDataNascimento() != null
                && cliente.getDataNascimento().after(new Date())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, 
                    "Data de Nascimento inválida!");
        }        
    }
    
}
