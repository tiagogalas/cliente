FROM openjdk:8

MAINTAINER "Tiago Galas tiagogalas@gmail.com"

RUN mkdir /opt/app

ADD ./target/cliente-*.jar /opt/app/api-cliente.jar 

CMD ["java", "-jar", "/opt/app/api-cliente.jar"]
